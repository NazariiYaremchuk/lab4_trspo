package com.yaremchuk.lab4.services;

import com.yaremchuk.lab4.dtos.ProductDto;
import com.yaremchuk.lab4.entities.Product;

import java.util.List;

public interface ProductService {
    List<ProductDto> getAll();

    Product findById(Long id);
}
