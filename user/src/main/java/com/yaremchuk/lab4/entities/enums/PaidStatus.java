package com.yaremchuk.lab4.entities.enums;

public enum PaidStatus {
    NOT_PAID, PAID
}
