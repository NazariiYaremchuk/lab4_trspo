package com.yaremchuk.lab4.security.services;


import com.yaremchuk.lab4.security.dtos.AddUserRequestDto;
import com.yaremchuk.lab4.security.dtos.LoginDto;
import com.yaremchuk.lab4.security.dtos.UserDto;
import com.yaremchuk.lab4.security.entities.User;

public interface UserService {
    UserDto save(AddUserRequestDto addUserRequestDto);

    User findByUsername(String username);

    UserDto authenticateUser(LoginDto loginDto);
}
