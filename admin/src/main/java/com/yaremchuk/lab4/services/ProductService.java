package com.yaremchuk.lab4.services;

import com.yaremchuk.lab4.dtos.AddProductDto;
import com.yaremchuk.lab4.dtos.AddQuantityOfProductDto;
import com.yaremchuk.lab4.dtos.ProductDto;
import com.yaremchuk.lab4.entities.Product;

import java.util.List;

public interface ProductService {
    List<ProductDto> getAll();

    ProductDto updateProduct(AddQuantityOfProductDto addQuantityOfProductDto);

    ProductDto addProduct(AddProductDto addProductDto);

    void deleteProduct(Long id);

    Product findById(Long id);
}
